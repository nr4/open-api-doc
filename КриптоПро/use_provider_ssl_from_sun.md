После того как был пропатчен jdk библиотеками КриптоПро, необходимо отключить ГОСТ алгоритмы
для SSL, в файле по адресу: `$JDK_HOME/jre/lib/security`

**Отключаем CryptoPro для ssl**
```
# security.provider.14=ru.CryptoPro.ssl.Provider
```
**Отключаем алгоритмы КриптоПро для ssl**
```
# ssl.KeyManagerFactory.algorithm=GostX509
# ssl.TrustManagerFactory.algorithm=GostX509
```
**Подключаем стандартные алгоритмы Sun для SSL, путем добавления следующих строк**
```
ssl.KeyManagerFactory.algorithm=SunX509
ssl.TrustManagerFactory.algorithm=PKIX
```