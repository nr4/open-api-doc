На компьютере разработчика, запустить `ControlPanel` из командной строки:
 `jcp-2.0.39014>ControlPane.bat "c:\Program Files (x86)\Java\jdk1.8.0_121"`
`c:\Program Files (x86)\Java\jdk1.8.0_121` - адрес JDK, куда производилась установка КриптоПро.

В окне ControlPanel, перейти на вкладку *Hardware: Path to HDImage store*
КриптоПро, контейнер с сертификатами по умолчанию - HDImageStore
`Windows ${user.home}\Local Settings\Application Data\Crypto Pro`

В Linux, адрес размещения, хранилища по умолчанию: `/var/opt/cprocsp/keys/<username>`
